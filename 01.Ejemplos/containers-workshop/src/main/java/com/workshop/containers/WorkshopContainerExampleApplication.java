package com.workshop.containers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopContainerExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkshopContainerExampleApplication.class, args);
    }

}
