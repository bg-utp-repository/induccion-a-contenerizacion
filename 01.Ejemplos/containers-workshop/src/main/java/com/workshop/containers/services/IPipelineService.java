package com.workshop.containers.services;

import com.workshop.containers.entities.Pipeline;

import java.util.List;

public interface IPipelineService {
    public List<Pipeline> getListOfPipelines();
    public Pipeline findPipelineById(int id);
}
