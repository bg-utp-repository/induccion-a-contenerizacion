# ⚡️ 🐳 Inducción a contenerización de aplicaciones

### 📍Contenido: 
 00.Material/00.PPT : Presentación  
 
 01.Ejemplos/containers-workshop : Aplicacion en Java sencilla con su Dockerfile. Esta imagen la puede descargar tambien desde el [Repositorio](https://hub.docker.com/repository/docker/localhost8027/containers-workshop-apirest/general) y simplemente correrla siguiendo las instrucciones. 

 Para crear la imagen en local, debe pararse dentro de la carpeta y ejecutar el siguiente comando `docker-compose up -d`
 
 01.Ejemplos/containers-workshop-2 : Aplicacion en Python sencilla con su Dockerfile.

 Para crear la imagen en local, debe pararse dentro de la carpeta y ejecutar el siguiente comando `docker build -t python-container-workshop:124 .` y luego para correr el contenedor debe correr el siguiente comando `docker run --publish 8080:5000 python-container-workshop:124`


>> Si desea revisar aplicaciones un poco mas complejas, puede descargar este [Repositorio](https://github.com/carlosmayorga/flask-apirest). Es un Apirest en Python, el cual corre en un contenedor y se conecta a otro contenedor de Mysql. 
